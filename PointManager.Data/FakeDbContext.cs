﻿using System.Collections.Generic;
namespace PointManager.Data
{
    public class FakeDbContext
    {
        public FakeDbContext()
        {
            GenerateFakeData();
        }

        public List<CameraPosition> CameraPositions { get; set; }

        private void GenerateFakeData()
        {
            CameraPositions = new List<CameraPosition>()
            {
                new CameraPosition() { Id = 1, PositionName =  "Alfa", CameraX = 1, CameraY = 2,  CameraZ = 0, CameraDegreeHorizontal = 30, CameraDegreeVertical = 31 },
                new CameraPosition() { Id = 1, PositionName =  "Beta", CameraX = 2, CameraY = 3,  CameraZ = 1, CameraDegreeHorizontal = 40, CameraDegreeVertical = 41 },
                new CameraPosition() { Id = 1, PositionName = "Delta", CameraX = 3, CameraY = 4,  CameraZ = 2, CameraDegreeHorizontal = 50, CameraDegreeVertical = 51 },
                new CameraPosition() { Id = 1, PositionName = "Gamma", CameraX = 4, CameraY = 5,  CameraZ = 3, CameraDegreeHorizontal = 60, CameraDegreeVertical = 61 },
            };
        }      
    }
}
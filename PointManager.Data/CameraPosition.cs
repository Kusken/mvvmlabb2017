﻿namespace PointManager.Data
{
    public class CameraPosition
    {
        public int Id { get; set; }
        public string PositionName { get; set; }
        public double CameraX { get; set; }
        public double CameraY { get; set; }
        public double CameraZ { get; set; }
        public double CameraDegreeHorizontal { get; set; }
        public double CameraDegreeVertical { get; set; }
    }
}
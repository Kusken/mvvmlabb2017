﻿using System;
using System.Windows.Input;

namespace PointManager.Commands
{
    public abstract class BaseCommand : ICommand
    {
        private Predicate<object> canExecute;
        private Action<object> execute;

        public BaseCommand(Action<object> exectue, Predicate<object> canExecute)
        {
            execute = exectue;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            var b = canExecute?.Invoke(parameter) ?? true;
            return b;
        }

        public void Execute(object parameter)
        {
            execute(parameter);
        }


        public event EventHandler CanExecuteChanged;
    }
}


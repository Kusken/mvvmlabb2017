﻿using System;

namespace PointManager.Commands
{
    public class World3DWindowLoadedCommand : BaseCommand
    {
        public World3DWindowLoadedCommand(Action<object> exectue, Predicate<object> canExecute)
            : base(exectue, canExecute) { }
    }
}
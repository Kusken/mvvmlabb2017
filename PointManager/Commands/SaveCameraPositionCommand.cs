﻿using System;
using System.Windows.Input;

namespace PointManager.Commands
{
    public class SaveCameraPositionCommand : ICommand
    {
        private Action exectue;

        public SaveCameraPositionCommand(Action updateCameraPosition)
        {
            exectue = updateCameraPosition;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            exectue();
        }

        public event EventHandler CanExecuteChanged;
    }
}
﻿using System;

namespace PointManager.Abstractions
{
    public interface ITimer
    {
        TimeSpan Interval { get; set; }

        event EventHandler Tick;
        
        void Start();
        
        void Stop();
    }
}
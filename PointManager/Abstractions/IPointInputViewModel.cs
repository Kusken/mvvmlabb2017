﻿using System.Windows.Input;
using PointManager.Models;

namespace PointManager.Abstractions
{
    public interface IPointInputViewModel
    {
        CameraPosition ModelCameraPosition { get; set; }
        ICommand SaveCameraPositionCommand { get; set; }
    }
}
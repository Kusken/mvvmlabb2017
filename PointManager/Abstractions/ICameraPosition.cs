﻿using System.Windows.Media.Media3D;

namespace PointManager.Abstractions
{
    public interface ICameraPosition
    {
        double DegreesHorizontal { get; set; }
        double DegreesVertical { get; set; }
        Point3D Look { get; }
        Point3D Position { get; set; }
        double X { get; set; }
        double Y { get; set; }
        double Z { get; set; }

        void Move(double distance);
        void Strafe(double distance);
    }
}
﻿using PointManager.Annotations;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Media3D;

namespace PointManager.Abstractions
{
    public class World3DCamera : INotifyPropertyChanged, IWorld3DCamera
    {
        private ProjectionCamera projectionCamera;


        private World3DCamera(PerspectiveCamera projectionCamera)
        {
            this.projectionCamera = projectionCamera;
        }

        public static World3DCamera CreateWorld3DCameraFromPerspectiveCamera(PerspectiveCamera camera)
            => new World3DCamera(camera);


        ProjectionCamera IWorld3DCamera.ProjectionCamera => projectionCamera;


        Point3D IWorld3DCamera.Position
        {
            get { return projectionCamera.Position; }
            set
            {
                projectionCamera.Position = value;
                OnPropertyChanged();
            }
        }

        Vector3D IWorld3DCamera.LookDirection
        {
            get { return projectionCamera.LookDirection; }
            set
            {
                projectionCamera.LookDirection = value;
                OnPropertyChanged();
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
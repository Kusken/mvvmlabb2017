﻿using System.Windows;
using System.Windows.Input;

namespace PointManager.Abstractions
{
    public interface IWorld3DViewModel
    {
        IWorld3DCamera Camera { get; }
        ICameraPosition CameraPosition { get; set; }
        IMovement Movement { get; }
        ITimer Timer { get; set; }
        ICommand WindowLoadedCommand { get; set; }

        void ExecuteKeyPressedOperationFromKeyEventArgs(KeyEventArgs keyReleased);
        void ExecuteKeyReleasedOperationFromKeyEventArgs(KeyEventArgs keyPressed);
        void SetCameraAnglesFromCurrentMouseCoordinatePointAndYAxisCenterPointAndWindowWidth(Point point, double yAxisCenterPoint, double windowWidth);
    }
}
﻿using System.Windows.Input;
using PointManager.Utilites;

namespace PointManager.Abstractions
{
    public interface IMovement
    {
        double Steps { get; set; }
        Throttle Strafe { get; set; }
        Throttle Walk { get; set; }
        Throttle None { get; set; }

        void InitiateDepthMovementFromPressedKeyboardKey(Key key);
        void HaltDepthMovementFromReleasedKeyboardKey(Key key);
    }
}
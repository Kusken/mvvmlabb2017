﻿using System.ComponentModel;
using System.Windows.Media.Media3D;

namespace PointManager.Abstractions
{
    public interface IWorld3DCamera
    {
        ProjectionCamera ProjectionCamera { get; }

        Vector3D LookDirection { get; set; }
        Point3D Position { get; set; }
    }
}
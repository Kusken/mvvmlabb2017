﻿using PointManager.Temporary;
using PointManager.ViewModels;
using System.Windows;
using System.Windows.Input;

namespace PointManager.Views
{
    public partial class World3DView : Window
    {
        public World3DView()
        {
            InitializeComponent();
            Viewport3D.Camera = ViewModelLocator.World3DViewModel.Camera.ProjectionCamera;
            (new MazeGenerator()).MakeMaze(m3Dg);
        }


        private void Window1_KeyDown(object sender, KeyEventArgs e) =>
            ViewModelLocator.World3DViewModel.ExecuteKeyPressedOperationFromKeyEventArgs(e);

        private void Window1_KeyUp(object sender, KeyEventArgs e) =>
            ViewModelLocator.World3DViewModel.ExecuteKeyReleasedOperationFromKeyEventArgs(e);


        private void World3DView_OnMouseMove(object sender, MouseEventArgs e)
        {
            var yAxisCenterPoint = ActualHeight / 2;

            ViewModelLocator.World3DViewModel
                .SetCameraAnglesFromCurrentMouseCoordinatePointAndYAxisCenterPointAndWindowWidth
                (e.GetPosition(null), yAxisCenterPoint, ActualWidth);
        }
    }
}
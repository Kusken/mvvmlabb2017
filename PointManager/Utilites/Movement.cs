using PointManager.Abstractions;
using PointManager.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace PointManager.Utilites
{
    public class Movement : INotifyPropertyChanged, IMovement
    {

        private IDictionary<Key, Action> keyPressedToHorizontalAndDepthMovementMapper;

        private Dictionary<Key, Action> keyReleasedToHorizontalAndDepthMovementMapper;

        private Throttle walk;
        private Throttle strafe;
        private Throttle none;
        private double steps;

        

        

        public Movement(double steps)
        {
            this.steps = steps;
            InitializeMovementMappers();
        }

        public Throttle Walk
        {
            get { return walk; }
            set
            {
                walk = value;
                OnPropertyChanged();
            }
        }

        public Throttle Strafe
        {
            get { return strafe; }
            set
            {
                strafe = value;
                OnPropertyChanged();
            }
        }

        public double Steps
        {
            get { return steps; }
            set
            {
                steps = value;
                OnPropertyChanged();
            }
        }

        public Throttle None
        {
            get { return none; }
            set
            {
                none = value;
                OnPropertyChanged();
            }
        }

        private void InitializeMovementMappers()
        {
            

            keyPressedToHorizontalAndDepthMovementMapper = new Dictionary<Key, Action>
            {
                {Key.Up, WalkForward},
                {Key.Down, WalkBackward},
                {Key.Left, StrafeLeft},
                {Key.Right, StrafeRight},
            };

            keyReleasedToHorizontalAndDepthMovementMapper = new Dictionary<Key, Action>
            {
                {Key.Up, HaltWalk},
                {Key.Down, HaltWalk},
                {Key.Left, HaltStrafe},
                {Key.Right, HaltStrafe}
            };
        }





        public void SetDepthMovementThrottleFromPressedKeyboardKey(Key key)
        {
            if (KeyPressedToHorizontalAndDepthMovementMapperContainsKey(key))
                InvokeSetWalkOrStrafeActionFromPressedKey(key);
        }

        public void SetDepthMovementFromReleasedKeyboardKey(Key key)
        {
            if (KeyReleasedToHorizontalAndDepthMovementMapperContainsKey(key))
                InvokeWalkOrStrafeActionFromReleasedKey(key);
        }



        private bool KeyPressedToHorizontalAndDepthMovementMapperContainsKey(Key key)
            => keyPressedToHorizontalAndDepthMovementMapper.ContainsKey(key);

        private bool KeyReleasedToHorizontalAndDepthMovementMapperContainsKey(Key key) 
            => keyReleasedToHorizontalAndDepthMovementMapper.ContainsKey(key);


        private void InvokeSetWalkOrStrafeActionFromPressedKey(Key key) =>
            keyPressedToHorizontalAndDepthMovementMapper[key].Invoke();

        private void InvokeWalkOrStrafeActionFromReleasedKey(Key key) =>
            keyReleasedToHorizontalAndDepthMovementMapper[key].Invoke();



        private void WalkBackward() => Walk = Throttle.Negative;

        private void WalkForward() => Walk = Throttle.Positive;

        private void StrafeRight() => Strafe = Throttle.Positive;

        private void StrafeLeft() => Strafe = Throttle.Negative;


        private void HaltWalk() => Walk = Throttle.None;

        private void HaltStrafe() => Strafe = Throttle.None;




        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
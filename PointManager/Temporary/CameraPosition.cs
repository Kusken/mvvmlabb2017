﻿using System;
using System.Windows.Media.Media3D;
using PointManager.Abstractions;
using PointManager.Models;

namespace PointManager.Temporary
{
    public class CameraPosition : ModelBase, ICameraPosition
    {
        private const double HalfPi = Math.PI / 180;
        private double degreesHorizontal, degreesVertical;
        private Point3D position;

        public Point3D Position
        {
            get { return position; }
            set
            {
                position = value;
                OnPropertyChanged();
            }
        }

        public double DegreesHorizontal
        {
            get { return degreesHorizontal; }
            set
            {
                degreesHorizontal = AngleInterval(value);
                OnPropertyChanged();
            }
        }

        public double DegreesVertical
        {
            get { return degreesVertical; }
            set
            {
                degreesVertical = AngleInterval(value);
                OnPropertyChanged();
            }
        }

        public double X
        {
            get { return position.X; }
            set
            {
                position.X = value; 
                OnPropertyChanged();
            }
        }

        public double Y
        {
            get { return position.Y; }
            set
            {
                position.Y = value; 
                OnPropertyChanged();
            }
        }

        public double Z
        {
            get { return position.Z; }
            set
            {
                position.Z = value;
                OnPropertyChanged();
            }
        }

        public Point3D Look
        {
            get
            {
                const int distance = 3;
                double x1 = Math.Sin(DegreesHorizontal * HalfPi) * distance,
                    z1 = Math.Cos(DegreesHorizontal * HalfPi) * distance;
                return new Point3D
                {
                    Y = Math.Sin(DegreesVertical * HalfPi) * distance,
                    Z = Math.Cos(DegreesVertical * HalfPi) * z1,
                    X = Math.Cos(DegreesVertical * HalfPi) * x1
                };
            }
        }

        public void Move(double distance)
        {
            X += Math.Sin(DegreesHorizontal * HalfPi) * distance;
            Z += Math.Cos(DegreesHorizontal * HalfPi) * distance;
        }

        public void Strafe(double distance)
        {
            var distanceX = Math.Sin(DegreesHorizontal * HalfPi) * distance;
            var distanceZ = Math.Cos(DegreesHorizontal * HalfPi) * distance;
            X += -distanceZ;
            Z += distanceX;
        }

        private double AngleInterval(double degree)
        {
            if (degree > 360) return degree - 360;
            if (degree < 0) return degree + 360;
            return degree;
        }
    }
}
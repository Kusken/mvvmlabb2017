﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using PointManager.Utilities;

namespace PointManager.Temporary
{
    public class MazeGenerator
    {
        private readonly double @base = 0;
        private readonly double top = 1.5;
        private readonly double degreesVertical = 0.04;

        public void MakeMaze(Model3DGroup m3Dg)
        {
            var baseImageBrush = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(Path.Combine(Environment.CurrentDirectory, "Images", "base.jpg"))),
                Opacity = 0.5
            };

            var topBmp = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(Path.Combine(Environment.CurrentDirectory, "Images", "top.jpg")))
            };

            var innerImageBrush = new ImageBrush
            {
                ImageSource =
                    new BitmapImage(new Uri(Path.Combine(Environment.CurrentDirectory, "Images", "Inner.jpg"))),
                Opacity = 0.65
            };

            var outerImageBrush = new ImageBrush
            {
                ImageSource =
                    new BitmapImage(new Uri(Path.Combine(Environment.CurrentDirectory, "Images", "Outer.jpg")))
            };
            //Outer_bmp.Opacity = 0.1;

            Material baseMaterial = new DiffuseMaterial(baseImageBrush);
            Material topMaterial = new DiffuseMaterial(topBmp);
            Material outerMaterial = new DiffuseMaterial(outerImageBrush);
            Material innerMaterial = new DiffuseMaterial(innerImageBrush);

            var baseMeshGeometry3D = CreateMg3(m3Dg, baseMaterial);
            var topMeshGeometry3D = CreateMg3(m3Dg, topMaterial);
            var outerMeshGeometry3D = CreateMg3(m3Dg, outerMaterial);
            var innerMeshGeometry3D = CreateMg3(m3Dg, innerMaterial);

            int minX = -15, minZ = -15;
            // base
            for (var x = minX; x <= 3; x += 2)
                for (var z = minZ; z <= 3; z += 2)
                    Triangulate(baseMeshGeometry3D, new Point3D(x, @base, z), new Point3D(x, @base, z + 2),
                        new Point3D(x + 2, @base, z + 2), new Point3D(x + 2, @base, z), new Point(0, 0), new Point(0, 1),
                        new Point(1, 1), new Point(1, 0));

            // top.
            for (var x = minX; x <= 4; x += 1)
                for (var z = minZ; z <= 4; z += 1)
                    Triangulate(topMeshGeometry3D, new Point3D(x, top, z), new Point3D(x + 1, top, z),
                        new Point3D(x + 1, top, z + 1), new Point3D(x, top, z + 1), new Point(0, 0), new Point(0, 1),
                        new Point(1, 1), new Point(1, 0));

            //var sky = top;
            //sky = 20;
            //// top.
            //for (int x = -150; x <= 150; x++)
            //    for (int z = -150; z <= 150; z++)
            //        Triangulate(Top_mg3, new Point3D(x, sky, z), new Point3D(x + 1, sky, z), new Point3D(x + 1, sky, z + 1), new Point3D(x, sky, z + 1), new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(1, 0));

            // Outer pair 1:2
            for (var x = minX; x <= 3; x += 2)
            {
                Triangulate(outerMeshGeometry3D, new Point3D(x, top, -5), new Point3D(x, @base, -5), new Point3D(x + 2, @base, -5),
                    new Point3D(x + 2, top, -5), new Point(0, 0), new Point(0, 1), new Point(0.5, 1), new Point(0.5, 0));
                Triangulate(outerMeshGeometry3D, new Point3D(x + 2, top, 5), new Point3D(x + 2, @base, 5), new Point3D(x, @base, 5),
                    new Point3D(x, top, 5), new Point(0, 0), new Point(0, 1), new Point(0.5, 1), new Point(0.5, 0));
            }

            // Outer pair 2:2
            for (var z = -5; z <= 3; z += 2)
            {
                Triangulate(outerMeshGeometry3D, new Point3D(5, top, z), new Point3D(5, @base, z), new Point3D(5, @base, z + 2),
                    new Point3D(5, top, z + 2), new Point(0, 0), new Point(0, 1), new Point(0.5, 1), new Point(0.5, 0));
                Triangulate(outerMeshGeometry3D, new Point3D(-5, top, z + 2), new Point3D(-5, @base, z + 2),
                    new Point3D(-5, @base, z), new Point3D(-5, top, z), new Point(0, 0), new Point(0, 1),
                    new Point(0.5, 1), new Point(0.5, 0));
            }

            var K = new List<int>
            {
                -1,
                -5,
                -1,
                -4,
                -2,
                -4,
                -2,
                -3,
                -1,
                -4,
                -1,
                -3,
                4,
                -4,
                4,
                -3,
                -2,
                -3,
                -2,
                -2,
                3,
                -3,
                3,
                -2,
                4,
                -3,
                4,
                -2,
                -3,
                -2,
                -3,
                -1,
                1,
                -2,
                1,
                -1,
                2,
                -2,
                2,
                -1,
                3,
                -2,
                3,
                -1,
                4,
                -2,
                4,
                -1,
                -3,
                -1,
                -3,
                0,
                -2,
                -1,
                -2,
                0,
                -1,
                -1,
                -1,
                0,
                0,
                -1,
                0,
                0,
                1,
                -1,
                1,
                0,
                2,
                -1,
                2,
                0,
                4,
                -1,
                4,
                0,
                -4,
                0,
                -4,
                1,
                -3,
                0,
                -3,
                1,
                -2,
                0,
                -2,
                1,
                0,
                0,
                0,
                1,
                1,
                0,
                1,
                1,
                2,
                0,
                2,
                1,
                3,
                0,
                3,
                1,
                4,
                0,
                4,
                1,
                -4,
                1,
                -4,
                2,
                -3,
                1,
                -3,
                2,
                -2,
                1,
                -2,
                2,
                -1,
                1,
                -1,
                2,
                1,
                1,
                1,
                2,
                2,
                1,
                2,
                2,
                3,
                1,
                3,
                2,
                -4,
                2,
                -4,
                3,
                -3,
                2,
                -3,
                3,
                -2,
                2,
                -2,
                3,
                2,
                2,
                2,
                3,
                4,
                2,
                4,
                3,
                -4,
                3,
                -4,
                4,
                -2,
                3,
                -2,
                4,
                -1,
                3,
                -1,
                4,
                3,
                3,
                3,
                4,
                4,
                3,
                4,
                4,
                -1,
                4,
                -1,
                5,
                4,
                4,
                4,
                5,
                -5,
                -3,
                -4,
                -3,
                -5,
                -1,
                -4,
                -1,
                -4,
                -4,
                -3,
                -4,
                -4,
                -3,
                -3,
                -3,
                -4,
                -2,
                -3,
                -2,
                -4,
                4,
                -3,
                4,
                -3,
                -4,
                -2,
                -4,
                -3,
                -2,
                -2,
                -2,
                -3,
                4,
                -2,
                4,
                -2,
                -2,
                -1,
                -2,
                -2,
                2,
                -1,
                2,
                -1,
                -3,
                -0,
                -3,
                -1,
                -2,
                -0,
                -2,
                -1,
                -1,
                -0,
                -1,
                -1,
                1,
                -0,
                1,
                -1,
                3,
                -0,
                3,
                0,
                -4,
                1,
                -4,
                0,
                -3,
                1,
                -3,
                0,
                -2,
                1,
                -2,
                0,
                2,
                1,
                2,
                0,
                3,
                1,
                3,
                0,
                4,
                1,
                4,
                1,
                -4,
                2,
                -4,
                1,
                -3,
                2,
                -3,
                1,
                4,
                2,
                4,
                2,
                -4,
                3,
                -4,
                2,
                -3,
                3,
                -3,
                2,
                3,
                3,
                3,
                2,
                4,
                3,
                4,
                3,
                -4,
                4,
                -4,
                3,
                2,
                4,
                2
            };
            for (var i = 0; i + 4 < K.Count; i += 4)
                ExtrudeSurface(ref innerMeshGeometry3D, new Surface { X1 = K[i], Z1 = K[i + 1], X2 = K[i + 2], Z2 = K[i + 3] });
        }

        private MeshGeometry3D CreateMg3(Model3DGroup model, Material image)
        {
            var tmp = new MeshGeometry3D();
            var newModel = new GeometryModel3D(tmp, image);
            model.Children.Add(newModel);
            return tmp;
        }

        private void Triangulate(MeshGeometry3D meshGeometry3D, Point3D a, Point3D b, Point3D c, Point3D d, Point m, Point n,
            Point o, Point p)
        {
            // Set points.
            meshGeometry3D.Positions.Add(a);
            meshGeometry3D.Positions.Add(b);
            meshGeometry3D.Positions.Add(c);
            meshGeometry3D.Positions.Add(d);

            // Set texture coords.
            meshGeometry3D.TextureCoordinates.Add(m);
            meshGeometry3D.TextureCoordinates.Add(n);
            meshGeometry3D.TextureCoordinates.Add(o);
            meshGeometry3D.TextureCoordinates.Add(p);

            // Define two triangles for each rectangle.
            var trianglePoint = meshGeometry3D.Positions.Count - 4;
            meshGeometry3D.TriangleIndices.Add(trianglePoint);
            meshGeometry3D.TriangleIndices.Add(trianglePoint + 1);
            meshGeometry3D.TriangleIndices.Add(trianglePoint + 2);
            meshGeometry3D.TriangleIndices.Add(trianglePoint);
            meshGeometry3D.TriangleIndices.Add(trianglePoint + 2);
            meshGeometry3D.TriangleIndices.Add(trianglePoint + 3);
        }

        private void ExtrudeSurface(ref MeshGeometry3D meshGeometry3D, Surface wall)
        {
            double xStep = 0;
            double zStep = 0;
            var direction = false;
            var xMin = Math.Min(wall.X1, wall.X2);
            var xMax = Math.Max(wall.X1, wall.X2);
            var zMin = Math.Min(wall.Z1, wall.Z2);
            var zMax = Math.Max(wall.Z1, wall.Z2);

            if (xMin == xMax)
            {
                xStep = degreesVertical / 2;
                direction = true;
            }

            if (zMin == zMax)
            {
                zStep = degreesVertical / 2;
                direction = false;
            }

            var a = new Point3D(xMin - xStep, top, zMin - zStep);
            var b = new Point3D(xMin - xStep, top, zMax + zStep);
            var c = new Point3D(xMax + xStep, top, zMax + zStep);
            var d = new Point3D(xMax + xStep, top, zMin - zStep);
            var e = a;
            var f = b;
            var g = c;
            var h = d;
            e.Y = @base;
            f.Y = @base;
            g.Y = @base;
            h.Y = @base;

            var m = new Point(0, 0);
            var n = new Point(0, 1);
            var o = new Point(1, 1);
            var p = new Point(1, 0);

            CreateSolid(meshGeometry3D, a, b, c, d, e, f, g, h, m, n, o, p, direction);
        }

        private void CreateSolid(MeshGeometry3D meshGeometry3D, Point3D a, Point3D b, Point3D c, Point3D d, Point3D e, Point3D f,
            Point3D g, Point3D h, Point m, Point n, Point o, Point p, bool direction)
        {
            if (direction)
            {
                Triangulate(meshGeometry3D, b, c, d, a, m, n, o, p);
                Triangulate(meshGeometry3D, g, f, e, h, m, n, o, p);
            }
            else
            {
                Triangulate(meshGeometry3D, a, b, c, d, m, n, o, p);
                Triangulate(meshGeometry3D, h, g, f, e, m, n, o, p);
            }
            Triangulate(meshGeometry3D, a, e, f, b, m, n, o, p);
            Triangulate(meshGeometry3D, c, g, h, d, m, n, o, p);

            Triangulate(meshGeometry3D, b, f, g, c, m, n, o, p);
            Triangulate(meshGeometry3D, d, h, e, a, m, n, o, p);
        }
    }
}
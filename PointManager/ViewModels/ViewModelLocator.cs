﻿using PointManager.Abstractions;

namespace PointManager.ViewModels
{
    public class ViewModelLocator
    {
        public static IWorld3DViewModel World3DViewModel { get; } = new World3DViewModel();

        public static IPointInputViewModel PointInputViewModel { get; } = new PointInputViewModel();
    }
}
﻿using PointManager.Abstractions;
using PointManager.Commands;
using PointManager.Models;
using PointManager.Utilites;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using CameraPosition = PointManager.Models.CameraPosition;
using PerspectiveCamera = System.Windows.Media.Media3D.PerspectiveCamera;

namespace PointManager.ViewModels
{
    public class World3DViewModel : ViewModelBase, IWorld3DViewModel
    {
        private ITimer timer;
        private ICameraPosition cameraPositionModel;
        private IWorld3DCamera camera;

        private ICommand windowLoadedCommand;


        public World3DViewModel()
        {
            camera = World3DCamera.CreateWorld3DCameraFromPerspectiveCamera(new PerspectiveCamera());
            cameraPositionModel = new Temporary.CameraPosition();

            Movement = new Movement(1);

            LoadCommands();
            LoadModel();
        }



        public ICommand WindowLoadedCommand
        {
            get { return windowLoadedCommand; }
            set { windowLoadedCommand = value; }
        }

        public ICameraPosition CameraPosition
        {
            get { return cameraPositionModel; }
            set
            {
                cameraPositionModel = value;
                OnPropertyChanged();
            }
        }

        public IWorld3DCamera Camera => camera;


        public ITimer Timer
        {
            get { return timer; }
            set
            {
                timer = value;
                OnPropertyChanged();

                
            }
        }

        public IMovement Movement { get; }



        private void LoadModel()
        {
            CameraPosition = new Temporary.CameraPosition();
        }



        private void LoadCommands()
        {
            WindowLoadedCommand = new World3DWindowLoadedCommand(InitializeViewModel, CanInitializeViewModel);
        }

        private bool CanInitializeViewModel(object obj) => true;

        private void InitializeViewModel(object obj)
        {
            InitializeCamera();
            InitializeTimer();
        }
        
        private void InitializeCamera()
        {
            CameraPosition = new Temporary.CameraPosition { X = 1, Y = 0.5, Z = 0 };

            Camera.Position = CameraPosition.Position;

            Camera.LookDirection = new Vector3D(
                CameraPosition.Look.X,
                CameraPosition.Look.Y,
                CameraPosition.Look.Z);
        }

        private void InitializeTimer()
        {
            Timer = new Timer() { Interval = TimeSpan.FromMilliseconds(16) };
            Timer.Tick += UpdateCameraMovement;
            Timer.Start();
        }

        

        public void SetCameraAnglesFromCurrentMouseCoordinatePointAndYAxisCenterPointAndWindowWidth
            (Point point, double yAxisCenterPoint, double windowWidth)
        {
            point = IsCursorPositionAboveWindowCenter(point, windowWidth)
                ?
                CalculateVerticalAngleFromMouseCursorPointingAboveWindowCenter(point, yAxisCenterPoint)
                :
                CalculateVerticalAngleFromMouseCursorPonitingBelowWindowCenter(point, yAxisCenterPoint);

            CalculateCameraHorizonalAngle(point, windowWidth);
        }

        private bool IsCursorPositionAboveWindowCenter(Point point, double yAxisCenterPoint) => point.Y > yAxisCenterPoint;


        private Point CalculateVerticalAngleFromMouseCursorPonitingBelowWindowCenter(Point point, double yAxisCenterPoint)
        {
            var proc = point.Y / yAxisCenterPoint;

            var lowerWindowPositioningUpperLimit = 90;

            CameraPosition.DegreesVertical = lowerWindowPositioningUpperLimit - lowerWindowPositioningUpperLimit * proc;

            return point;
        }

        private Point CalculateVerticalAngleFromMouseCursorPointingAboveWindowCenter(Point point, double yAxisCenterPoint)
        {
            var proc = (point.Y - yAxisCenterPoint) / yAxisCenterPoint;

            var upperWindowPositioningUpperLimit = 360;
            var upperWindowPositioningLowerLimit = 90;

            CameraPosition.DegreesVertical = upperWindowPositioningUpperLimit - upperWindowPositioningLowerLimit * proc;

            return point;
        }


        private void CalculateCameraHorizonalAngle(Point point, double windowWidth)
        {
            var proc = point.X / windowWidth;
            CameraPosition.DegreesHorizontal = 720 - 720 * proc;
        }




        private void UpdateCameraMovement(object sender, EventArgs e)
        {
            if (IsWalking())
                SetCameraDepthMovementParameters();

            if (IsStrafing())
                SetCameraSidewaysMovementParameters();

            MoveCameraToNewPosition();
            SetCameraDirection();


        }



        private bool IsWalking() => Movement.Walk != Throttle.None;

        private bool IsStrafing() => Movement.Strafe != Throttle.None;


        private void SetCameraDepthMovementParameters() => 
            CameraPosition.Move((double)Movement.Walk * Movement.Steps * 0.1);

        private void SetCameraSidewaysMovementParameters() => CameraPosition.Strafe((double)Movement.Strafe * Movement.Steps * 0.1);


        private void MoveCameraToNewPosition() => Camera.Position = CameraPosition.Position;

        private void SetCameraDirection() => Camera.LookDirection = new Vector3D(CameraPosition.Look.X,
                CameraPosition.Look.Y, CameraPosition.Look.Z);
        



        public void ExecuteKeyPressedOperationFromKeyEventArgs(KeyEventArgs keyPressed)
        {
            if (keyPressed.Key == Key.Z)
                CameraPosition.Y += 0.1;

            if (keyPressed.Key == Key.X)
                CameraPosition.Y -= 0.1;

            
            Movement.InitiateDepthMovementFromPressedKeyboardKey(keyPressed.Key);

        }

        public void ExecuteKeyReleasedOperationFromKeyEventArgs(KeyEventArgs keyReleased) => 
            Movement.HaltDepthMovementFromReleasedKeyboardKey(keyReleased.Key);


    }
}
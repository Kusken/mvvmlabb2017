﻿using PointManager.Data;
using PointManager.Services;
using System.Collections.ObjectModel;

namespace PointManager.ViewModels
{
    public class PointNavigationViewModel : ViewModelBase
    {
        public PointNavigationViewModel()
        {
            LoadData();
        }

        public ObservableCollection<CameraPosition> CameraPositions { get; set; }

        public ICameraPositionRepository CameraRepository { get; set; }

        private void LoadData()
        {
            CameraRepository = new CameraPositionRepository();
            CameraPositions = new ObservableCollection<CameraPosition>(CameraRepository.GetCameraPositions());
        }

    }
}
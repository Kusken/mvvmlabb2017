﻿using System;
using PointManager.Models;
using System.Windows.Input;
using PointManager.Abstractions;
using PointManager.Commands;
namespace PointManager.ViewModels
{
    public class PointInputViewModel : ViewModelBase, IPointInputViewModel
    {
        public PointInputViewModel()
        {
            System.Diagnostics.Debug.WriteLine("MainViewModel instans skapad: "+ DateTime.Now);
            LoadModelCameraPosition();
            InitializeCommands();
        }

        private CameraPosition modelCameraPosition;

        public CameraPosition ModelCameraPosition
        {
            get { return modelCameraPosition; }
            set
            {
                modelCameraPosition = value;
                OnPropertyChanged(); 
            }
        }

        private void LoadModelCameraPosition()
        {
            ModelCameraPosition = new CameraPosition()
            {
                Id = 1,
                PositionName = "Origo",
                CameraX = 0.35,
                CameraY = 0,
                CameraZ = 0,
                CameraDegreeHorizontal = 0,
                CameraDegreeVertical = 0
            };
        }



        private ICommand saveCameraPositionCommand;

        public ICommand SaveCameraPositionCommand
        {
            get { return saveCameraPositionCommand; }
            set
            {
                saveCameraPositionCommand = value;
                OnPropertyChanged(); 
                
            }
        }


        private void UpdateCameraPositionFunction()
        {
            ModelCameraPosition.PositionName += " * ";
        }

        private void InitializeCommands()
        {
            SaveCameraPositionCommand = new SaveCameraPositionCommand(UpdateCameraPositionFunction);
        }


    }
}
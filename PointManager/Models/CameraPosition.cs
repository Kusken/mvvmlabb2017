﻿
namespace PointManager.Models
{
    public class CameraPosition : ModelBase
    {
        public int Id { get; set; }
        private string positionName;
        public string PositionName
        {
            get
            {
                return positionName;
            }
            set
            {
                positionName = value;
                OnPropertyChanged();
            }
        }

        private double cameraX;
        public double CameraX { get { return cameraX; }
            set
            {
                cameraX = value;
                OnPropertyChanged();
            }
        }


        private double cameraY;
        public double CameraY { get { return cameraY; }
            set
            {
                cameraY = value;
                OnPropertyChanged();
            }
        }


        private double cameraZ;
        public double CameraZ { get { return cameraZ; }
            set
            {
                cameraZ = value;
                OnPropertyChanged();
            }
        }


        private double cameraDegreeHorizontal;
        public double CameraDegreeHorizontal { get { return cameraDegreeHorizontal; }
            set
            {
                cameraDegreeHorizontal = value;
                OnPropertyChanged();
            }
        }


        private double cameraDegreeVertical;
        public double CameraDegreeVertical { get { return cameraDegreeVertical; }
            set
            {
                cameraDegreeVertical = value;
                OnPropertyChanged();
            }
        }
    }
}
